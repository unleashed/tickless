/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "License").
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets "[]" replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright (c) 2006, 2010, Oracle and/or its affiliates. All rights reserved.
 */

#ifndef _SOLARIS_KSH_CMDLIST_H
#define	_SOLARIS_KSH_CMDLIST_H

#ifdef	__cplusplus
extern "C" {
#endif

/*
 * List builtins for Solaris.
 * The list here is partially autogenerated and partially hand-picked
 * based on compatibility with the native Solaris versions of these
 * tools
 */

/*
 * Commands which are 100% compatible with native Solaris versions (/bin is
 * a softlink to ./usr/bin, ksh93 takes care about the lookup)
 */
#define	BINCMDLIST(f)	\
	{ "/bin/"	#f, NV_BLTIN|NV_BLTINOPT|NV_NOFREE, bltin(f) },
#define	USRBINCMDLIST(f)	\
	{ "/usr/bin/"	#f, NV_BLTIN|NV_BLTINOPT|NV_NOFREE, bltin(f) },
#define	SBINCMDLIST(f)	\
	{ "/sbin/"	#f, NV_BLTIN|NV_BLTINOPT|NV_NOFREE, bltin(f) },
#define	SUSRBINCMDLIST(f)	\
	{ "/usr/sbin/"	#f, NV_BLTIN|NV_BLTINOPT|NV_NOFREE, bltin(f) },
/* POSIX compatible commands */
#ifdef SHOPT_USR_GNU_BIN_BUILTINS
/* GNU coreutils compatible commands */
#define	GNUCMDLIST(f)	\
	{ "/usr/gnu/bin/" #f, NV_BLTIN|NV_BLTINOPT|NV_NOFREE, bltin(f) },
#else
#define	GNUCMDLIST(f)
#endif
/*
 * Make all ksh93 builtins accessible when /usr/ast/bin was added to
 * /usr/ccs/bin:/usr/bin:/bin:/opt/SUNWspro/bin
 */
#define	ASTCMDLIST(f)	\
	{ "/usr/ast/bin/" #f, NV_BLTIN|NV_BLTINOPT|NV_NOFREE, bltin(f) },

/* undo ast_map.h #defines to avoid collision */
#undef basename
#undef dirname
#undef mktemp

/* Generated data, do not edit. */
ASTCMDLIST(basename)
GNUCMDLIST(basename)
ASTCMDLIST(cat)
BINCMDLIST(cat)
ASTCMDLIST(chgrp)
ASTCMDLIST(chmod)
ASTCMDLIST(chown)
BINCMDLIST(chown)
ASTCMDLIST(cksum)
BINCMDLIST(cksum)
GNUCMDLIST(cksum)
ASTCMDLIST(cmp)
BINCMDLIST(cmp)
ASTCMDLIST(comm)
BINCMDLIST(comm)
GNUCMDLIST(comm)
ASTCMDLIST(cp)
ASTCMDLIST(cut)
BINCMDLIST(cut)
GNUCMDLIST(cut)
ASTCMDLIST(date)
ASTCMDLIST(dirname)
BINCMDLIST(dirname)
GNUCMDLIST(dirname)
ASTCMDLIST(egrep)
ASTCMDLIST(expr)
GNUCMDLIST(expr)
ASTCMDLIST(fds)
ASTCMDLIST(fgrep)
ASTCMDLIST(fmt)
ASTCMDLIST(fold)
BINCMDLIST(fold)
GNUCMDLIST(fold)
ASTCMDLIST(grep)
ASTCMDLIST(head)
BINCMDLIST(head)
ASTCMDLIST(id)
ASTCMDLIST(join)
BINCMDLIST(join)
GNUCMDLIST(join)
ASTCMDLIST(ln)
ASTCMDLIST(logname)
BINCMDLIST(logname)
GNUCMDLIST(logname)
ASTCMDLIST(md5sum)
ASTCMDLIST(mkdir)
BINCMDLIST(mkdir)
GNUCMDLIST(mkdir)
ASTCMDLIST(mkfifo)
BINCMDLIST(mkfifo)
GNUCMDLIST(mkfifo)
ASTCMDLIST(mktemp)
BINCMDLIST(mktemp)
GNUCMDLIST(mktemp)
ASTCMDLIST(mv)
ASTCMDLIST(paste)
BINCMDLIST(paste)
GNUCMDLIST(paste)
ASTCMDLIST(pathchk)
BINCMDLIST(pathchk)
GNUCMDLIST(pathchk)
ASTCMDLIST(readlink)
ASTCMDLIST(rev)
BINCMDLIST(rev)
ASTCMDLIST(rm)
ASTCMDLIST(rmdir)
BINCMDLIST(rmdir)
GNUCMDLIST(rmdir)
GNUCMDLIST(sleep)
ASTCMDLIST(stty)
ASTCMDLIST(sum)
BINCMDLIST(sum)
ASTCMDLIST(sync)
BINCMDLIST(sync)
GNUCMDLIST(sync)
SBINCMDLIST(sync)
SUSRBINCMDLIST(sync)
ASTCMDLIST(tail)
BINCMDLIST(tail)
ASTCMDLIST(tee)
BINCMDLIST(tee)
GNUCMDLIST(tee)
ASTCMDLIST(tty)
BINCMDLIST(tty)
GNUCMDLIST(tty)
ASTCMDLIST(uname)
ASTCMDLIST(uniq)
BINCMDLIST(uniq)
GNUCMDLIST(uniq)
ASTCMDLIST(wc)
BINCMDLIST(wc)
GNUCMDLIST(wc)
ASTCMDLIST(xgrep)
BINCMDLIST(xgrep)

/* Mandatory for ksh93 test suite and AST scripts */
BINCMDLIST(getconf)

#ifdef	__cplusplus
}
#endif

#endif /* !_SOLARIS_KSH_CMDLIST_H */
